from types import SimpleNamespace
import random
import os
import sys

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import telegram

import yaml
import logging

from sosongo import Sosongo

class Sos():
    def __init__(self, pattern, cons):
        self.pattern = pattern
        self.cons = cons

    def __str__(self):
        return pattern

class DepeshaBot():
    def __init__(self, config):
        self.config = config
        self.updater = Updater(config['token'])
        self.dispatcher = self.updater.dispatcher

        self.dispatcher.add_handler(CommandHandler('start', self.start))
        self.dispatcher.add_handler(CommandHandler('depesha', self.depesha))
        self.dispatcher.add_handler(CommandHandler('sos', self.sos))
        self.dispatcher.add_handler(CommandHandler('debil', self.debil))
        self.dispatcher.add_handler(CommandHandler('undebil', self.undebil))
        self.dispatcher.add_handler(CommandHandler('debils', self.debils))
        self.dispatcher.add_handler(CommandHandler('mem', self.mem))
        self.dispatcher.add_handler(MessageHandler(Filters.text, self.text))

        print("Debils: {}".format(self.config['debils']))

        self.sosongo = Sosongo()


    def start(self, bot, update):
        update.message.reply_text(text='yoba', parse_mode=telegram.ParseMode.MARKDOWN)

    def depesha(self, bot, update):
        try:
            print('depesha command recieved')
            if update.message.from_user.id in self.config['admins']:
                target = update.message.reply_to_message.from_user.id
                print("target: {}".format(target))

                if target is not None:
                    self.config['recipients'].append(target)
                    
                    with open(os.path.expanduser('config.yml'), 'w') as f:
                        yaml.dump(self.config, f)

                    print("{} added to recipients".format(update.message.reply_to_message.from_user.id))
        except Exception as e:
            print(e)

    def text(self, bot, update):
        try:
            user_id = update.message.from_user.id
            if user_id in self.config['recipients']:
                answer = random.choice(self.config['answers'])
                update.message.reply_text(text=str(answer), parse_mode=telegram.ParseMode.MARKDOWN)

            if user_id in self.config['debils']:
                print('debil {} found'.format(user_id))
                #self._evil_twin(bot, update, user_id)
                #text = update.message.text
                result = self._sosongo(user_id, update.message.text)
                update.message.reply_text(text=result, parse_mode=telegram.ParseMode.MARKDOWN)

        except Exception as e:
            print(e)

    def _gen_sosongo(self, word_len):
        cons_p = []
        cons_p.append('sos')
        cons_p.append('b')
        cons_p.append('ng')
        cons_p.append('r')
        cons_p.append('c')

        vow_p = []
        vow_p.append('a')
        vow_p.append('i')
        vow_p.append('o')

        result = []

        init_dice = random.randint(1,2)
        result.append(random.choice(cons_p) if init_dice == 1 else random.choice(vow_p))

        for i in range(1, word_len):
            if result[i-1] in cons_p:
                result.append(random.choice(vow_p))
            else:
                result.append(random.choice(cons_p))

        return ''.join(result)

    def _sosongo(self, user_id, text):
        text = text.lower()

        words = text.split(' ')

        ac = 10

        result = []

        for word in words:
            dice = random.randint(1,20)
            if dice > ac:
                sos = self._gen_sosongo(len(word))
                result.append(self.sosongo.sosongo(sos))
            else:
                result.append(word)

        return ' '.join(result)

    def _evil_twin(self, bot, update, user_id):
        answering_debil = random.choice(self.config['debils'])
        with open('debils/{}.txt'.format(answering_debil), 'r+') as d:
            answers = d.readlines()
            answer = random.choice(answers) if len(answers) > 0 else update.message.text
            print("debil's answer: "+answer)
            update.message.reply_text(text=str(answer), parse_mode=telegram.ParseMode.MARKDOWN)
            if answering_debil == user_id:
                d.write(update.message.text+'\n')
            else:
                with open('debils/{}.txt'.format(user_id), 'r+') as cd:
                    cd.write(update.message.text+'\n')


    def debil(self, bot, update):
        if update.message.from_user.id in self.config['admins']:
            target = update.message.reply_to_message.from_user.id
            text = update.message.reply_to_message.text
            print("target: {}".format(target))

            if target is not None:
                self.config['debils'].append(target)

                if not os.path.exists('debils/{}.txt'.format(target)):
                    with open('debils/{}.txt'.format(target), 'w') as d:
                        d.write(text+'\n')

                with open(os.path.expanduser('config.yml'), 'w') as f:
                    yaml.dump(self.config, f)
                    print("{} added to debils".format(update.message.reply_to_message.from_user.id))
                    update.message.reply_text(text='Дебил добавлен', parse_mode=telegram.ParseMode.MARKDOWN)

    def undebil(self, bot, update):
        if update.message.from_user.id in self.config['admins']:
            target = update.message.reply_to_message.from_user.id
            print("target: {}".format(target))
            if target is not None:
                self.config['debils'].remove(target)
                with open(os.path.expanduser('config.yml'), 'w') as f:
                    yaml.dump(self.config, f)
                    print("{} removed from debils".format(update.message.reply_to_message.from_user.id))
                    update.message.reply_text(text='Дебил избавлен от злой участи', parse_mode=telegram.ParseMode.MARKDOWN)


    def debils(self, bot, update):
        debils = self.config['debils']
        update.message.reply_text(text=str(debils), parse_mode=telegram.ParseMode.MARKDOWN)

    def mem(self, bot, update):
        try:
            if update.message.from_user.id in self.config['admins']:
                text = update.message.reply_to_message.text
                user_id = update.message.reply_to_message.from_user.id
                if not os.path.exists('debils/{}.txt'.format(user_id)):
                    with open('debils/{}.txt'.format(user_id), 'w') as d:
                        d.write(text+'\n')

                with open('debils/{}.txt'.format(user_id), 'r+') as d:
                    d.write(text)

                print("{} added to debil's ({}) db".format(text, user_id))

                update.message.reply_text(text='В мемориз!', parse_mode=telegram.ParseMode.MARKDOWN)
        except Exception as e:
            print(e)


    
    def sos(self, bot, update):
        msg = update.message.text[4:].lower()

        print(msg)
        
        try:
            res = self.sosongo.sosongo(msg)
            print(res)
            update.message.reply_text(res)
        except Exception as e:
            print(e)
        
	
    def start_polling(self):
        self.updater.start_polling()


def main(config):
	bot = DepeshaBot(config)
	bot.start_polling()

if __name__ == '__main__':
	with open(os.path.expanduser('config.yml'), 'r') as f:
		try:
			config = yaml.load(f)
		except yaml.YAMLError:
			print('incorrect format')
			exit(1)

	log = logging.getLogger('bot')
	logger = logging.StreamHandler(sys.stdout)
	logger.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
	logger.setLevel(logging.DEBUG)
	log.addHandler(logger)
	log.setLevel(logging.DEBUG)

	main(config)
