class Sosongo():
    def __init__(self):
        self.letters = {
            'з' : '\U0001F4A4',
            'г' : '\U0001F3D7',
            'л' : '\U0001F6F4',
            'е' : '\U0001F4E7',
            'ё' : '\U0001F4E7',
            'э' : '\U0001F4E7',
            'т' : '\U0000271D',
            'к' : '\U0000262A',
            'у' : '\U000026CE',
            'х' : '\U00002716',
            'я' : '\U000000AE',
            'ж' : '\U0000002A\U0000FE0F\U000020E3',
            'а' : '\U0001F170',
            'б' : '\U0001F171',
            'м' : '\U000024C2',
            'о' : '\U0001F17E',
            'р' : '\U0001F17F',
            'п' : '\U0000264A',
            'и' : '\U00002139',
            'й' : '\U00002139',
            'с' : '\U0001F4B2',
            'д' : '\U0001F364',
            'ш' : '\U0001F3B9',
            'щ' : '\U00002668',
            'н' : '\U0001F3E5',
            'в' : '\U00002648',
            'ф' : '\U0001F004',
            'ч' : '\U0000262A\U0001F3E5',
            'ц' : '\U0001FA7A',
            'ы' : '\U0001F3BC',
            'ь' : '\U0001F300',
            'ъ' : '\U0001F300',
            '!' : '\U00002757',
            '?' : '\U00002753',
            ' ' : ' ',
            'a' : chr(0x1F170), 
            'b' : chr(0x1F171), 
            'i' : chr(0x2139), 
            'm' : chr(0x24C2),
            'o' : chr(0x1F17E),
            'p' : chr(0x1F17F),
            'r' : chr(0x00AE),
            'c' : chr(0x00A9),
        }

        self.sequences = {
                's' : ['sos', chr(0x1F198)],
                'o' : ['ok', chr(0x1F197)],
                'n' : ['ng', chr(0x1F196)],
                'i' : ['id', chr(0x1F194)],
                'a' : ['ab', chr(0x1F18E)],
        }

        self.unknown = '\U0001F413'

    def _letter(self, c):
        if c.isdigit():
            return chr(0x0030+int(c)) + chr(0xFE0F) + chr(0x20E3)
        elif c in self.letters:
            return self.letters[c]
        else:
            return self.unknown 


    def sosongo(self, string):
        res = ''

        i = 0
        while i < len(string):
            c = string[i]

            if c in self.sequences:
                seq = self.sequences[c][0]
                msg = self.sequences[c][1]
                if i+len(seq) <= len(string) and string[i:i+len(seq)] == seq:
                    res += msg
                    i += len(seq)
                else:
                    res += self._letter(c) 
                    i += 1
            else:
                res += self._letter(c)
                i += 1
            
        return res


